# BOHT - teh bot for you !
#
#

"""console."""

import sys
import threading
import boht
import boht.exception
import boht.handler

def init(kernel):
    c = boht.console.Console()
    c.start()
    c.wait()
    return c

class Console(boht.handler.Handler):

    def __init__(self):
        super().__init__()
        self._connected = threading.Event()
        self._ready = threading.Event()
        self._threaded = False
        
    def announce(self, txt):
        self.raw(txt)

    def poll(self):
        self._connected.wait()
        e = Event()
        e.etype = "command"
        e.origin = "root@shell"
        e.orig = repr(self)
        e.txt = input("> ")
        if not e.txt:
            raise lo.exp.ENOTXT 
        return e

    def input(self):
        while not self._stopped:
            try:
                e = self.poll()
            except lo.exp.ENOTXT:
                continue
            except EOFError:
                break
            boht.dispatch(self, e)
            e.wait()
        self._ready.set()

    def raw(self, txt):
        sys.stdout.write(str(txt) + "\n")
        sys.stdout.flush()

    def say(self, channel, txt, type="chat"):
        self.raw(txt)

    def start(self, handler=False, input=True):
        if self.error:
            return
        super().start(handler)
        if input:
            boht.threads.launch(self.input)
        self._connected.set()

    def wait(self):
        if self.error:
            return
        self._ready.wait()
