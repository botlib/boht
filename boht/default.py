# BOHT - teh bot for you !
#
#

import boht

default_irc = {
    "channel": "#%s" % (lo.cfg.name or "boht"),
    "nick": lo.cfg.name or "boht",
    "ipv6": False,
    "port": 6667,
    "server": "localhost",
    "ssl": False,
    "realname": lo.cfg.name or "boht",
    "username": lo.cfg.name or "boht"
}

default_krn = {
    "workdir": "",
    "kernel": False,
    "modules": "",
    "options": "",
    "prompting": True,
    "dosave": False,
    "level": "",
    "logdir": "",
    "shell": False
}

default_rss = {
    "display_list": "title,link",
    "dosave": True,
    "tinyurl": False
}

defaults = boht.Object()
defaults["irc"] = boht.Object(default_irc)
defaults["rss"] = boht.Object(default_rss)
defaults["kernel"] = boht.Object(default_krn)
