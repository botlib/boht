# BOHT - teh bot for you !
#
#

import os
import boht

def __dir__():
    return ("ed", "find")

k = boht.get_kernel()

def list_files(wd):
    return "|".join([x for x in os.listdir(os.path.join(wd, "store"))])

def ed(event):
    if not event.args:
        event.reply(list_files(lo.workdir) or "no files yet")
        return
    cn = event.args[0]
    shorts = k.find_shorts("boht")
    cn = shorts.get(cn, cn)    
    db = boht.Db()
    l = db.last(cn)
    if not l:     
        try:
            c = boht.typ.get_cls(cn)
            l = c()
            dft = boht.defaults.defaults.get(cn)
            l.update(dft)
            event.reply("created %s" % cn)
        except boht.exception.ENOCLASS:
            event.reply(list_files(boht.workdir) or "no files yet")
            return
    if len(event.args) == 1:
        event.reply(l)
        return
    if len(event.args) == 2:
        event.reply(l.get(event.args[1]))
        return
    setter = {event.args[1]: event.args[2]}
    l.edit(setter)
    p = l.save()
    event.reply("ok %s" % p)
