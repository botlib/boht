# setup.py

from setuptools import setup

def read():
    return open("README.rst", "r").read()

def list_bin():
    return os.listdir("bin")

setup(
    name='boht',
    version='3',
    url='https://bitbucket.org/botlib/botdev',
    author='Bart Thate',
    author_email='bthate@dds.nl',
    description=""" BOHT - teh bot for you ! """,
    long_description=read(),
    long_description_content_type="text/x-rst",
    license='Public Domain',
    zip_safe=True,
    packages=["boht"],
    scripts=list_bin(),
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
